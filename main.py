#!/usr/bin/env python3

from tkinter import *


class Window(Frame):

    values = []

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master

        # widget can take all window
        self.pack(fill=BOTH, expand=1)

        self.text = StringVar()
        self.text.set("")
        self.lb = Label(self, textvariable=self.text)
        self.lb.pack()

        # number buttons

        zeroButton = Button(self, text="0", height=3,
                            width=10, bg='#567', fg='White', command=lambda: self.addValue(0))
        zeroButton.place(x=175, y=675)
        zeroButton.size()

        oneButton = Button(self, text="1", height=3,
                           width=10, bg='#567', fg='White', command=lambda: self.addValue(1))
        oneButton.place(x=75, y=600)
        oneButton.size()

        twoButton = Button(self, text="2", height=3,
                           width=10, bg='#567', fg='White', command=lambda: self.addValue(2))
        twoButton.place(x=175, y=600)
        twoButton.size()

        threeButton = Button(self, text="3", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue(3))
        threeButton.place(x=275, y=600)
        threeButton.size()

        fourButton = Button(self, text="4", height=3,
                            width=10, bg='#567', fg='White', command=lambda: self.addValue(4))
        fourButton.place(x=75, y=525)
        fourButton.size()

        fiveButton = Button(self, text="5", height=3,
                            width=10, bg='#567', fg='White', command=lambda: self.addValue(5))
        fiveButton.place(x=175, y=525)
        fiveButton.size()

        sixButton = Button(self, text="6", height=3,
                           width=10, bg='#567', fg='White', command=lambda: self.addValue(6))
        sixButton.place(x=275, y=525)
        sixButton.size()

        sevenButton = Button(self, text="7", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue(7))
        sevenButton.place(x=75, y=450)
        sevenButton.size()

        eightButton = Button(self, text="8", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue(8))
        eightButton.place(x=175, y=450)
        eightButton.size()

        nineButton = Button(self, text="9", height=3,
                            width=10, bg='#567', fg='White', command=lambda: self.addValue(9))
        nineButton.place(x=275, y=450)
        nineButton.size()

        # operation buttons
        clearButton = Button(self, text="CE", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue("CE"))
        clearButton.place(x=75, y=675)
        clearButton.size()

        equalButton = Button(self, text="=", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue("="))
        equalButton.place(x=275, y=675)
        equalButton.size()

        plusButton = Button(self, text="+", height=3,
                            width=10, bg='#567', fg='White', command=lambda: self.addValue("+"))
        plusButton.place(x=375, y=525)
        plusButton.size()

        minusButton = Button(self, text="-", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue("-"))
        minusButton.place(x=375, y=450)
        minusButton.size()

        multiButton = Button(self, text="*", height=3,
                             width=10, bg='#567', fg='White', command=lambda: self.addValue("*"))
        multiButton.place(x=375, y=600)
        multiButton.size()

        divideButton = Button(self, text="/", height=3,
                              width=10, bg='#567', fg='White', command=lambda: self.addValue("/"))
        divideButton.place(x=375, y=675)
        divideButton.size()

    def addValue(self, val):
        self.text.set(str(self.text.get()) + str(val))
        if (type(val) is int):
            try:
                if (isinstance(self.values[len(self.values)-1], str)):
                    self.values.append(val)
                else:
                    self.values[len(
                        self.values)-1] = str(self.values[len(self.values)-1]) + str(val)
                    self.values[len(self.values) -
                                1] = int(self.values[len(self.values)-1])
            except:
                self.values.append(val)
        elif (type(val) is str):
            if (val == "CE"):
                self.values.clear()
                self.text.set("")
            elif (val == "="):
                if (self.values[1] == "+"):
                    self.text.set(self.text.get() +
                                  str(self.values[0] + self.values[2]))
                elif (self.values[1] == "-"):
                    self.text.set(self.text.get() +
                                  str(self.values[0] - self.values[2]))
                elif (self.values[1] == "*"):
                    self.text.set(self.text.get() +
                                  str(self.values[0] * self.values[2]))
                elif (self.values[1] == "/"):
                    self.text.set(self.text.get() +
                                  str(self.values[0] / float(self.values[2])))
                self.values.clear()
            else:
                self.values.append(val)
        print("val" + str(self.values))


root = Tk()
app = Window(root)
root.wm_title("Tkinter Calculator")
root.geometry("500x750")
root.mainloop()
